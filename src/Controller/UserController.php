<?php

namespace Controller;

use General\Container;
use Model\User;

class UserController extends AbstractController
{
    public function loginAction()
    {
        if ($this->session->isLoggedIn())
        {
            $this->redirect('/courses');
        }

        if ($this->isValidLoginForm())
        {
            $username = $_POST['username'];
            $password = $_POST['password'];

            $repository = Container::getUserRepository();
            $user = $repository->userExist($username, $password);

            if ($user)
            {
                $this->session->logUser($user);
                $this->redirect('/courses');
            }
            else
            {
                $this->session->setMessage('error', 'Invalid credentials.');
            }
        }

        $this->display('index.tpl');
    }

    public function logoutAction()
    {
        if ($this->session->isLoggedIn())
        {
            $this->session->logout();
            $this->redirect("/login");
        }
        else {
            $this->redirect("/login");
        }
    }

    public function registerAction()
    {
        $user = new User();

        if ($this->isValidUserForm($user)) {
            $repository = Container::getUserRepository();
            $result = $repository->add($user);

            if (!$result) {
                $this->session->setMessage('error', $this->db->errno . '<br/>' .  $this->db->error);
            } else {
                $this->session->setMessage('success', 'You have been registered successfuly.');
                $this->redirect("/courses");
            }
        }

        $this->display('register.tpl');

    }


    private function isValidLoginForm()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $username = $password = "";

            if (empty($_POST["username"])) {
                $usernameErr = "Username is required.";
                $this->smarty->assign('usererr', $usernameErr);
            }

            if (empty($_POST["password"])) {
                $passwordErr = "password is required.";
                $this->smarty->assign('passworderr', $passwordErr);
            }

            if (!isset($usernameErr) && !isset($passwordErr)) {
                return true;
            }
        }

        return false;
    }

    private function isValidUserForm(User $user)
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $username = $firstname = $lastname = $email = $password = $birth = "";

            if (empty($_POST["username"])) {
                $usernameErr = "Username is required";
                $this->smarty->assign('usererr', $usernameErr);
            } else {
                $username = $_POST['username'];
            }

            if (empty($_POST["firstname"])) {
                $firstnameErr = "Firstname is required";
                $this->smarty->assign('firstnameerr', $firstnameErr);
            } else {
                $firstname = $_POST['firstname'];
            }

            if (empty($_POST["lastname"])) {
                $lastnameErr = "Lastname is required";
                $this->smarty->assign('lastnameerr', $lastnameErr);
            } else {
                $lastname = $_POST['lastname'];
            }

            if (empty($_POST["email"])) {
                $emailErr = "Email is required";
                $this->smarty->assign('emailerr', $emailErr);
            } else {
                $email = $_POST['email'];
            }

            if (empty($_POST["birth"])) {
                $birthErr = "Birth date is required";
                $this->smarty->assign('birtherr', $birthErr);
            } else {
                $birth = $_POST['birth'];
            }

            if (empty($_POST["password"])) {
                $passwordErr = "Password is required";
                $this->smarty->assign('passworderr', $passwordErr);
            } else {
                $password = $_POST['password'];
            }


                $employee = $_POST['employee'];


            if (!isset($usernameErr) && !isset($firstnameErr) && !isset($lastnameErr) && !isset($emailErr) && !isset($passwordErr) && !isset($birthErr)) {
                $user->setUsername($username);
                $user->setFirstname($firstname);
                $user->setLastname($lastname);
                $user->setBirth($birth);
                $user->setEmail($email);
                $user->setPassword($password);
                $user->setIsemployee($employee);

                return true;
            }
        }

        return false;
    }

}