<?php

namespace Controller;

class DefaultController extends AbstractController
{
    public function indexAction()
    {

        $this->display('index.tpl');
    }
}