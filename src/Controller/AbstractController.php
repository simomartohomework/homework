<?php

namespace Controller;

use General\Container;

class AbstractController
{
    protected $smarty;
    protected $db;
    protected $session;

    public function __construct()
    {
        $this->smarty = Container::getSmarty();
        $this->db = Container::getDb();
        $this->session = Container::getSession();
    }

    public function redirect($path)
    {
        header("Location: " . BASE_PATH . $path);
        exit;
    }

    public function display($templatePath)
    {
        $this->smarty->assign('session', $this->session);
        $this->smarty->display($templatePath);
    }

    protected function checkIsLoggedIn()
    {
        if (!$this->session->isLoggedIn())
        {
            $this->session->setMessage('error', 'You must be logged in to use the courses system.');
            $this->redirect('/login');
            return false;
        }
        return true;
    }
}