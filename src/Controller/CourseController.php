<?php

namespace Controller;

use General\Container;
use General\Session;
use Model\Course;
use Model\User;
use Repository\CourseRepository;


class CourseController extends AbstractController
{

    public function listAction()
    {
        $this->checkIsLoggedIn();
        $repository = Container::getCourseRepository();
        $courses = $repository->getAll();


        $this->smarty->assign('courses', $courses);
        $this->display('course/courses.tpl');
    }

    public function addAction()
    {
        $this->checkIsLoggedIn();
        $course = new Course();

        if ($this->isValidCourseForm($course)) {
            $repository = Container::getCourseRepository();
            $repository->add($course);
            $this->session->setMessage('success', 'Course added successfully.');
            $this->redirect("/courses");
        }

        $this->display('course/add.tpl');

    }

    public function editAction()
    {
        $this->checkIsLoggedIn();
        $repository = Container::getCourseRepository();
        $course = $repository->getById($_GET['id']);

        if ($this->isValidCourseForm($course)) {
            $repository->update($course);
            $this->session->setMessage('success', 'Course edited successfully.');
            $this->redirect("/courses");
        }

        $this->smarty->assign('course', $course);
        $this->display('course/edit.tpl');
    }

    public function deleteAction()
    {
        $this->checkIsLoggedIn();
        $repository = Container::getCourseRepository();
        $repository->deleteById($_GET['id']);
        $this->session->setMessage('success', 'Course deleted successfully.');
        $this->redirect("/courses");
    }

    private function isValidCourseForm(Course $course)
    {
        $this->checkIsLoggedIn();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $title = $lector = $major = $year = "";

            if (empty($_POST["title"])) {
                $titleErr = "Title is required";
                $this->smarty->assign('titleerr', $titleErr);
            } else {
                $title = $_POST['title'];
            }

            if (empty($_POST["lector"])) {
                $lectorErr = "Lector is required";
                $this->smarty->assign('lectorerr', $lectorErr);
            } else {
                $lector = $_POST['lector'];
            }

            if (empty($_POST["major"])) {
                $majorErr = "Major is required";
                $this->smarty->assign('majorerr', $majorErr);
            } else {
                $major = $_POST['major'];
            }

            if (empty($_POST["year"])) {
                $yearErr = "Year is required";
                $this->smarty->assign('yearerr', $yearErr);
            } else {
                $year = $_POST['year'];
            }

            if (!isset($yearErr) && !isset($majorErr) && !isset($lectorErr) && !isset($titleErr)) {
                $course->setTitle($title);
                $course->setLector($lector);
                $course->setMajor($major);
                $course->setYear($year);

                return true;
            }
        }

        return false;
    }


}