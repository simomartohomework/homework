<!DOCTYPE html>
<html lang="en">
<head>
    {include file="includes/head.tpl"}
</head>

<body>

{include file="includes/header.tpl"}

<div class="container">

    <div class="starter-template">
        <h1>Editing course:</h1>
        <p class="lead">{$course->getTitle()}</p><br/>
        <a class="btn btn-default" href="{$base_path}/courses">Back to List courses</a><br/>
    </div>

    <form class="form-horizontal" method="post">
        <div class="form-group">
            <label for="inputTitle" class="col-sm-2 control-label">Title</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputTitle" name="title" value="{$course->getTitle()}">
            </div>
        </div>
        <div class="form-group">
            <label for="inputLector" class="col-sm-2 control-label">Lector</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputLector" name="lector" value="{$course->getLector()}">
            </div>
        </div>        <div class="form-group">
            <label for="inputMajor" class="col-sm-2 control-label">Major</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputMajor" name="major" value="{$course->getMajor()}">
            </div>
        </div>
        <div class="form-group">
            <label for="inputYear" class="col-sm-2 control-label">Year</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputYear" name="year" value="{$course->getYear()}">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Edit course</button>
            </div>
        </div>
    </form>

</div><!-- /.container -->


{include file="includes/footer.tpl"}
</body>
</html>
