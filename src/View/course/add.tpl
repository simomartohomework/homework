<!DOCTYPE html>
<html lang="en">
<head>
    {include file="includes/head.tpl"}
</head>

<body>

{include file="includes/header.tpl"}

<div class="container">

    <div class="starter-template">
        <h1>Adding courses</h1>
        <p class="lead">Fill the requiered fields below</p><br/>
        <a class="btn btn-default" href="{$base_path}/courses">Back to List courses</a><br/>
    </div>

    <form class="form-horizontal" method="POST" action="">
        <div class="form-group {if isset($titleerr)}has-error{/if}">
            <label for="inputTitle" class="col-sm-2 control-label">Title</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputTitle" name="title" value="{$smarty.post.title|default:""}">
                <span id="helpBlock2" class="help-block">{if isset($titleerr)}{$titleerr}{/if}</span>
            </div>
        </div>
        <div class="form-group {if isset($lectorerr)}has-error{/if}">
            <label for="inputLector" class="col-sm-2 control-label">Lector</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputLector" name="lector" value="{$smarty.post.lector|default:""}">
                <span id="helpBlock2" class="help-block">{if isset($lectorerr)}{$lectorerr}{/if}</span>
            </div>
        </div>
        <div class="form-group {if isset($majorerr)}has-error{/if}">
            <label for="inputMajor" class="col-sm-2 control-label">Major</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputMajor" name="major" value="{$smarty.post.major|default:""}">
                <span id="helpBlock2" class="help-block"> {if isset($majorerr)}{$majorerr}{/if}</span>
            </div>
        </div>
        <div class="form-group {if isset($yearerr)}has-error{/if}">
            <label for="inputYear" class="col-sm-2 control-label">Year</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputYear" name="year" value="{$smarty.post.year|default:""}">
                <span id="helpBlock2" class="help-block">{if isset($yearerr)}{$yearerr}{/if}</span>

            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Add course</button>
            </div>
        </div>
    </form>

</div>

{include file="includes/footer.tpl"}
</body>
</html>
