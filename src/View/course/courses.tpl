<!DOCTYPE html>
<html lang="en">
<head>
    {include file="includes/head.tpl"}
</head>

<body>

{include file="includes/header.tpl"}

<div class="container">

    <div class="starter-template">
        {if $session->isLoggedIn()}
        <h1>Welcome, {$session->getUser()->getUsername()}</h1>
        {/if}
        <p class="lead"></p><br/>
        <a class="btn btn-default" href="{$base_path}/logout">Logout</a><br/>
    </div>

    <div class="starter-template">
        <h1>List of available courses</h1>
        <p class="lead"></p><br/>
        <a class="btn btn-default" href="{$base_path}/courses/add">Add course</a><br/>
    </div>

    {include file="includes/messages/success.tpl"}
    {include file="includes/messages/error.tpl"}

    <table class="table table-striped">
        <tr>
            <td>Title</td>
            <td>Lector</td>
            <td>Major</td>
            <td>Year</td>
            <td>Rate</td>
            {if $session->getUser()->getIsemployee()}  <td>Edit</td>
            <td>Delete</td>{/if}
        </tr>
        {foreach item=item from=$courses}
            <tr>
                <td>{$item->getTitle()}</td>
                <td>{$item->getLector()}</td>
                <td>{$item->getMajor()}</td>
                <td>{$item->getYear()}</td>
                <td>{$item->getRate()}</td>
                {if $session->getUser()->getIsemployee()}<td><a href="{$base_path}/courses/edit?id={$item->getId()}" class="btn btn-success">Edit</a></td>
                <td><a href="{$base_path}/courses/delete?id={$item->getId()}" class="btn btn-danger">Delete</a></td>{/if}
            </tr>
        {/foreach}
    </table>

</div><!-- /.container -->


{include file="includes/footer.tpl"}
</body>
</html>
