<!DOCTYPE html>
<html lang="en">
<head>
    {include file="includes/head.tpl"}
    <link href="{$base_path}/css/signin.css" rel="stylesheet">
</head>

<body>

{include file="includes/header.tpl"}

<div class="container">
    {include file="includes/messages/success.tpl"}
    {include file="includes/messages/error.tpl"}
    <br/><br/>
    {if $session->isLoggedIn()}
        TI SI LOGNAT
    {else}
        <form class="form-signin" action="{$base_path}/login" method="post">
            <h2 class="form-signin-heading">Please sign in to use the course system</h2>
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="text" id="username" name="username" class="form-control" placeholder="Username" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required><br/>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button><br/>
            <a class="btn btn-lg btn-primary btn-block" href="{$base_path}/register">Register</a><br/>
        </form>
    {/if}


</div> <!-- /container -->


{include file="includes/footer.tpl"}

</body>
</html>
