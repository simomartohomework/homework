{assign var="success_message" value=$session->getMessage('success')}

{if $success_message}
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {$success_message}
    </div>
{/if}