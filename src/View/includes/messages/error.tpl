{assign var="danger_message" value=$session->getMessage('error')}

{if $danger_message}
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Danger!</strong> {$danger_message}
    </div>
{/if}