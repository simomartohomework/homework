<!DOCTYPE html>
<html lang="en">
<head>
    {include file="includes/head.tpl"}
</head>

<body>

{include file="includes/header.tpl"}

<div class="container">

    {include file="includes/messages/success.tpl"}
    {include file="includes/messages/error.tpl"}

    <div class="starter-template">
        <h1>Registration</h1>
        <p class="lead">Fill the requiered fields below in order to register</p><br/>
    </div>

    <form class="form-horizontal" method="POST" action="">

        <div class="form-group {if isset($usererr)}has-error{/if}">
            <label for="username" class="col-sm-2 control-label">Username</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="username" name="username" value="{$smarty.post.username|default:""}">
                <span id="helpBlock2" class="help-block">{if isset($usererr)}{$usererr}{/if}</span>
            </div>
        </div>

        <div class="form-group {if isset($firstnameerr)}has-error{/if}">
            <label for="firstname" class="col-sm-2 control-label">Firstname</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="firstname" name="firstname" value="{$smarty.post.firstname|default:""}">
                <span id="helpBlock2" class="help-block">{if isset($firstnameerr)}{$firstnameerr}{/if}</span>
            </div>
        </div>
        <div class="form-group {if isset($lastnameerr)}has-error{/if}">
            <label for="lastname" class="col-sm-2 control-label">Lastname</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="lastname" name="lastname" value="{$smarty.post.lastname|default:""}">
                <span id="helpBlock2" class="help-block">{if isset($lastnameerr)}{$lastnameerr}{/if}</span>
            </div>
        </div>
        <div class="form-group {if isset($passworderr)}has-error{/if}">
            <label for="password" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="password" name="password" value="{$smarty.post.password|default:""}">
                <span id="helpBlock2" class="help-block"> {if isset($passworderr)}{$passworderr}{/if}</span>
            </div>
        </div>
        <div class="form-group {if isset($emailerr)}has-error{/if}">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="email" value="{$smarty.post.email|default:""}">
                <span id="helpBlock2" class="help-block">{if isset($emailerr)}{$emailerr}{/if}</span>

            </div>
        </div>
        <div class="form-group {if isset($birtherr)}has-error{/if}">
            <label for="birth" class="col-sm-2 control-label">Birthdate</label>
            <div class="col-sm-10">
                <input type="date" class="form-control" id="birth" name="birth" value="{$smarty.post.birth|default:""}">
                <span id="helpBlock2" class="help-block">{if isset($birtherr)}{$birtherr}{/if}</span>
            </div>
        </div>
        <div class="form-group {if isset($employeeerr)}has-error{/if}">
            <label for="employee" class="col-sm-2 control-label">Is employee</label>
            <div class="col-sm-10">
                <input type="checkbox" class="form-control" id="employee" name="employee" value="{$smarty.post.employee|default:""}">
                <span id="helpBlock2" class="help-block">{if isset($employeeerr)}{$employeeerr}{/if}</span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Register</button>
            </div>
        </div>
    </form>

</div>

{include file="includes/footer.tpl"}
</body>
</html>
