<?php

namespace General;

use Controller\CourseController;
use Controller\DefaultController;
use Controller\UserController;

class Router
{
    public function execute($path)
    {
        switch (true) {
            case $path == '/':
                (new UserController())->loginAction();
                break;
            case $path == '/login':
                (new UserController())->loginAction();
                break;
            case $path == '/register':
                (new UserController())->registerAction();
                break;
            case $path == '/logout':
                (new UserController())->logoutAction();
                break;
            case $path == '/courses':
                (new CourseController())->listAction();
                break;
            case $path == '/courses/add':
                (new CourseController())->addAction();
                break;
            case (strstr($path, '/courses/edit') !== false):
                (new CourseController())->editAction();
                break;
            case (strstr($path, '/courses/delete') !== false);
                (new CourseController())->deleteAction();
                break;
            default:
                header("HTTP/1.0 404 Not Found");
                echo "<h1>404 Not Found</h1>";
                echo "The page that you have requested could not be found.";
                break;
        }
    }
}