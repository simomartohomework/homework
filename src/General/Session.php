<?php


namespace General;


use Model\User;

class Session
{
    private $messageKey;

    private $userKey;

    public function __construct()
    {
        $this->messageKey = 'message_key_';
        $this->messageKey = 'user_key';
    }

    public function start()
    {
        session_start();
    }

    public function destroy()
    {
        session_destroy();
    }

    public function regenerate($delete_old_session = false)
    {
        session_regenerate_id($delete_old_session);
    }

    public function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function has($key)
    {
        return isset($_SESSION[$key]);
    }

    public function get($key, $default = null)
    {
        if ($this->has($key)) {
            return $_SESSION[$key];
        }

        return $default;
    }

    public function remove($key)
    {
        if ($this->has($key)) {
            unset($_SESSION[$key]);
        }
    }

    public function setMessage($type, $message)
    {
        $key = $this->messageKey . '_' . $type;

        $this->set($key, $message);
    }

    public function getMessage($type)
    {
        $key = $this->messageKey . '_' . $type;

        $message = $this->get($key);
        $this->remove($key);

        return $message;
    }

    public function logUser(User $user)
    {
        if ($this->isLoggedIn()) {
            throw new \Exception('There is logged in user already.');
        }

        $this->set($this->userKey, serialize($user));
    }

    public function isLoggedIn()
    {
        return $this->has($this->userKey);
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        if ($this->isLoggedIn()) {
            return unserialize($this->get($this->userKey));
        }

        return null;
    }

    public function logout()
    {
        $this->destroy();
    }
}