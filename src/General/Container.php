<?php

namespace General;

use Repository\CourseRepository;
use Repository\UserRepository;

class Container
{
    private static $data;

    public static function set($key, $value)
    {
        self::$data[$key] = $value;
    }

    public static function get($key, $default = null)
    {
        if (self::has($key)) {
            return self::$data[$key];
        }

        return $default;
    }

    public static function has($key)
    {
        return isset(self::$data[$key]);
    }

    /**
     * @return \Smarty
     */
    public static function getSmarty()
    {
        return self::get('smarty');
    }

    /**
     * @return \mysqli
     */
    public static function getDb()
    {
        return self::get('db');
    }

    /**
     * @return Session
     */
    public static function getSession()
    {
        return self::get('session');
    }

    /**
     * @return CourseRepository
     */
    public static function getCourseRepository()
    {
        if (self::has('course_repository')) {
            return self::get('course_repository');
        }

        $repository = new CourseRepository();

        self::set('course_repository', $repository);

        return $repository;
    }

    /**
     * @return UserRepository
     */
    public static function getUserRepository()
    {
        if (self::has('user_repository')) {
            return self::get('user_repository');
        }

        $repository = new UserRepository();

        self::set('user_repository', $repository);

        return $repository;
    }
}