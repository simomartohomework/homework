<?php


namespace Repository;


use General\Container;

abstract class AbstractRepository
{
    protected $db;

    abstract protected function getTableName();

    abstract protected function getClassName();

    public function __construct()
    {
        $this->db = Container::getDb();
    }

    /**
     * @return array
     */
    public function getAll()
    {
        $result = $this->db->query('SELECT * FROM ' . $this->getTableName());

        $data = $result->fetch_all(MYSQLI_ASSOC);

        $objects = array();

        foreach ($data as $row)
        {
            $className = $this->getClassName();

            $object = new $className();

            foreach ($row as $key => $value) {
                $method = 'set' . ucfirst($key);
                if (method_exists($object, $method)) {
                    $object->$method($value);
                }
            }

            $objects[] = $object;
        }

        return $objects;
    }

    public function getById($id)
    {
        $result = $this->db->query("SELECT * FROM " . $this->getTableName() . " WHERE `id` = " . $this->db->escape_string($id));

        if ($result->num_rows > 0) {
            $data = $result->fetch_assoc();

            $className = $this->getClassName();
            $object = new $className;

            foreach ($data as $key => $value) {
                $method = 'set' . ucfirst($key);
                if (method_exists($object, $method)) {
                    $object->$method($value);
                }
            }

            return $object;
        }

        return null;
    }

    public function deleteById($id)
    {
        $result = $this->db->query("DELETE FROM " . $this->getTableName() . " WHERE `id` = " . $this->db->escape_string($id));

        return $result;
    }
}