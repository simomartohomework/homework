<?php

namespace Repository;


use Model\Course;

/**
 * @method Course[] getAll()
 * @method Course getById($id)
 */
class CourseRepository extends AbstractRepository
{
    protected function getTableName()
    {
        return 'courses';
    }

    protected function getClassName()
    {
        return Course::class;
    }
    
    public function add(Course $course)
    {
        list($title, $lector, $major, $year, $rate) = $this->escapeCourseData($course);

        $result = $this->db->query("INSERT INTO courses (title, lector, major, `year`, rate) VALUES ('$title', '$lector', '$major', '$year', '$rate')");

        if ($result) {
            $course->setId($this->db->insert_id);
        }

        return $result;
    }

    public function update(Course $course)
    {
        list($title, $lector, $major, $year, $rate) = $this->escapeCourseData($course);

        $result = $this->db->query("UPDATE courses SET
            `title`='$title',
            `lector`='$lector',
            `major`='$major',
            `year`='$year',
            `rate`='$rate'
            WHERE id={$course->getId()};"
        );

        return $result;
    }

    /**
     * @param Course $course
     * @return array
     */
    private function escapeCourseData(Course $course)
    {
        return array(
            $this->db->escape_string($course->getTitle()),
            $this->db->escape_string($course->getLector()),
            $this->db->escape_string($course->getMajor()),
            $this->db->escape_string($course->getYear()),
            $this->db->escape_string($course->getRate()),
        );
    }
}