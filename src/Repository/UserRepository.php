<?php

namespace Repository;


use Model\User;

/**
 * @method User[] getAll()
 * @method User getById($id)
 */
class UserRepository extends AbstractRepository
{
    protected function getTableName()
    {
        return 'users';
    }

    protected function getClassName()
    {
        return User::class;
    }

    public function userExist($username, $password)
    {
        $username = $this->db->escape_string($username);
        $password = md5($password);

        $result = $this->db->query("SELECT * FROM `users` WHERE `username` = '$username' AND `password` = '$password'");

        if ($result->num_rows > 0) {
            $row = $result->fetch_array(MYSQLI_ASSOC);

            $className = $this->getClassName();

            $user = new $className();

            foreach ($row as $key => $value) {
                $method = 'set' . ucfirst($key);
                if (method_exists($user, $method)) {
                    $user->$method($value);
                }
            }

            return $user;

        }

        return false;
    }

    public function add(User $user)
    {
        list($firstname, $lastname, $username, $birth, $password, $email, $isemployee) = $this->escapeUserData($user);

        $result = $this->db->query("INSERT INTO users (`firstname`, `lastname`, `username`, `password`, `birth`, `email`, `isemployee`) VALUES ('$firstname', '$lastname', '$username', '$password', '$birth', '$email', '$isemployee')");

        if ($result) {
            $user->setId($this->db->insert_id);
        }

        return $result;
    }

    public function update(User $user)
    {
        list($firstname, $lastname, $username, $birth, $password, $email, $isemployee) = $this->escapeUserData($user);

        $result = $this->db->query("UPDATE users SET
            `firstname`='$firstname',
            `lastname`='$lastname',
            `username`='$username',
            `birth`='$birth',
            'password'='$password',
            `email`='$email',
            `isemployee`='$isemployee';
            WHERE id={$user->getId()};"
        );

        if ($result) {
            $user->setId($this->db->insert_id);
        }

        return $result;
    }

    /**
     * @param User $user
     * @return array
     */
    private function escapeUserData(User $user)
    {
        return array(
            $this->db->escape_string($user->getFirstname()),
            $this->db->escape_string($user->getLastname()),
            $this->db->escape_string($user->getUsername()),
            $this->db->escape_string($user->getBirth()),
            md5($user->getPassword()),
            $this->db->escape_string($user->getEmail()),
            $this->db->escape_string($user->getIsemployee())
        );
    }
}