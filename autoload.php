<?php

$basePath = __DIR__ . '/src/';

$myAutoload = function ($name) use ($basePath) {
    $filename = $basePath . str_replace('\\', '/', $name) . '.php';

    if (file_exists($filename)) {
        require_once $filename;
    }
};

spl_autoload_register($myAutoload);