<?php

use General\Container;
use General\Router;
use General\Session;

$dir = basename(__DIR__);
define('BASE_PATH', "http://{$_SERVER['HTTP_HOST']}/$dir");

function initializeSession()
{
    $session = new Session();
    $session->start();
    Container::set('session', $session);
}

function initializeSmarty()
{
    require_once('libs/smarty-3.1.29/libs/Smarty.class.php');
    $smarty = new Smarty();
    $smarty->setTemplateDir(__DIR__ . '/src/View/');
    $smarty->assign('base_path', BASE_PATH);
    Container::set('smarty', $smarty);
}

function initializeDb()
{
    $db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DATABASE);
    Container::set('db', $db);
}

function handleRouting()
{
    $url = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

    $basename = basename(__DIR__);

    $path = substr(strstr($url, $basename), strlen($basename));

    $router = new Router();

    $router->execute($path);
}