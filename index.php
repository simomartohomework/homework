<?php

require_once 'config.php';
require_once 'functions.php';
require_once 'autoload.php';

initializeSession();
initializeSmarty();
initializeDb();
handleRouting();